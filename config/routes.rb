Rails.application.routes.draw do
  root 'welcome#index'
  get 'welcome/result', to: 'welcome#result', as: 'result_path'

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
