# GitLab test app

## Versions used
* Rails version: 5.0.0.1
* Ruby version: 2.3.1 (x86_64-linux)


## Docker container
```shell
docker build -t app .
docker run -i -p 3000:3000 app
```
## Access to app
`http://<hostIpOrName>:3000`