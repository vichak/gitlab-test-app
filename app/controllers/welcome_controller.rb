require 'net/http'


class WelcomeController < ApplicationController
  def index
  end

  def result
    startTime = Time.now
    @url = 'https://gitlab.com'
    @from = "RENNES, FRANCE"
    over = 5*60 # seconds
    @totalRequests = 0
    totalResponseTime = 0
    @requests = Array.new
    while Time.now - startTime <= over do
      start = Time.now
      Net::HTTP.get(URI(@url))
      responseTime = Time.now - start
      @requests.push( responseTime )
      @totalRequests += 1
      totalResponseTime += responseTime 
    end
    @averageResponseTime = totalResponseTime / @totalRequests
  end
end
